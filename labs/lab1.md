# Lab 1 - Polyhedral Model

First, get [iscc](../resources/iscc.tgz) and update your environment variables ($PATH and $LD_LIBRARY_PATH). 

## Loop shifting
  Consider the code:

```c
for(i=1; i<=N; i++) {
  b[i] = a[i];
  c[i] = b[i-1];
}
```

1. Draw the iteration domain and the dependences.
1. Propose an affine schedule which exposes parallelism
1. Generate the code with iscc

## Loop skewing
  Consider the code:

```c
for(i=1; i<=N; i++)
  for(j=1; j<=N; j++)
    a[i] = a[i-1] + a[i] + a[i+1];
```

1. Draw the iteration domain and the dependences.
1. Propose an affine schedule which exposes parallelism
1. Generate the code with iscc
1. Using [code_template.cc](../resources/code_template.cc), instrument the code to print the iterations and verify the execution order.


## Loop fusion
  Consider the code:

```c
//y := Ax; z := By;
for(i=1; i<=N; i++)
  for(j=1; j<=N; j++)
S:  y[i] += A[i][j]*x[j];
for(i=1; i<=N; i++)
  for(j=1; j<=N; j++)
T:  z[i] += B[i][j]*y[j];
```

1. Draw the iteration domain and the dependences.
1. Propose an affine schedule which minimizes the latency
1. Generate the code with iscc


