# Lab 3 - Array Multibanking

## Matrix-vector product

  Consider the code:

```c
#define SIZE 32

void matvec(
	long A[SIZE][SIZE],
	long X[SIZE],
	long Y[SIZE])
{
FOR_I:
	for (long i = 0; i < SIZE; i++)
	{
#pragma HLS PIPELINE
		Y[i] = 0;
	FOR_J:
		for (long j = 0; j < SIZE; j++)
		{
			Y[i] += A[i][j] * X[j];
		}
	}
}
```

1. Assuming a parallel execution of loop i, give the banking and the offset functions for each array
1. With VivadoHLS:
   1. Create a project, enter the code, set the pipeline pragma
   1. Set the ARRAY_PARTITION pragma


## Matrix-matrix product

  Consider the code:

```c
#define SIZE 32

void matmul(
	long A[SIZE][SIZE],
	long B[SIZE][SIZE],
	long C[SIZE][SIZE])
{
FOR_I:
	for (long i = 0; i < SIZE; i++)
	{
#pragma HLS PIPELINE
	FOR_J:
		for (long j = 0; j < SIZE; j++)
		{
		FOR_K:
			for (long k = 0; k < SIZE; k++)
			{
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}
}
```

1. Assuming a parallel execution of loops i and j, give the banking and the offset functions for each array
1. With VivadoHLS:
   1. Create a project, enter the code, set the pipeline pragma
   1. Set the ARRAY_PARTITION pragma




## 2D convolution

  Consider the code:

```c
#define SIZE 32

void conv2d(long matrix[SIZE][SIZE], long result[SIZE - 2][SIZE - 2])
{
FOR_I:
    for (long i = 1; i < SIZE - 1; i++)
    {
#pragma HLS PIPELINE
    FOR_J:
        for (long j = 1; j < SIZE - 1; j++)
        {
            result[i - 1][j - 1] =
                matrix[i - 1][j - 1] +
                matrix[i - 1][j] +
                matrix[i - 1][j + 1] +
                matrix[i][j - 1] +
                matrix[i][j] +
                matrix[i][j + 1] +
                matrix[i + 1][j - 1] +
                matrix[i + 1][j] +
                matrix[i + 1][j + 1];
        }
    }
}
```

1. Assuming a pipelined execution of loop i, give the banking and the offset functions for each array
1. With VivadoHLS:
   1. Create a project, enter the code, set the pipeline pragma
   1. Set the ARRAY_PARTITION pragma



