# Lab 2 - Array Contraction

## 1D Jacobi

  Consider the code:

```c
for(t=1; t<=T; t++)
  for(i=1; i<=N-2; i++)
    A[t][i] = (A[t-1][i-1] + A[t-1][i] + A[t-1][i+1])/3;
```

1. Do we need all the lines at the same time?
1. Draw the conflict polyhedron and apply the contraction algorithm


## 2D Blur Filter

  Consider the code:

```c
void blur_2d(double in[N][N], double out[N][N]) {
  double blurx[N][N];
  for(y=0; y<2; y++)
    for(x=2; x<N; x++)
  S:  blurx[y][x] = in[y][x]+in[y][x-1]+in[y][x-2];
  for(y=2; y<N; y++)
    for(x=2; x<N; x++) {
  T:  blurx[y][x] = in[y][x]+in[y][x-1]+in[y][x-2];
  U:  out[y][x] = blurx[y][x]+blurx[y-1][x]+blurx[y-2][x];
    }
}
```

1. Which array can we contract?
1. Draw the conflict polyhedron and apply the contraction algorithm
1. How can we reduce further the footprint?

