# Compiling circuits with polyhedra - Research seminar, UBO

## Supports

[All the slides](cours/handout.pdf)

- [Lecture 0. Introduction to Polyhedral HLS](cours/cours-0-intro.pdf)
- [Lecture 1. Polyhedral Model](cours/cours-1-poly.pdf) [[Lab]](labs/lab1.md)
- [Lecture 2. Array Contraction](cours/cours-2-contraction.pdf) [[Lab]](labs/lab2.md)
- [Lecture 3. Array Multibanking](cours/cours-3-multibanking.pdf) [[Lab]](labs/lab3.md)

## Resources
- [Iscc tool](resources/iscc.tgz)
- [More details](https://gitlab.inria.fr/alias/cours-m2-cr14)
